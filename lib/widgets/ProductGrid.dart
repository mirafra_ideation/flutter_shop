import 'package:flutter/material.dart';
import 'package:flutter_shops/provider/Products.dart';
import 'ProductItem.dart';
import 'package:provider/provider.dart';

class ProductGrid extends StatelessWidget {
  final bool showfev;

  ProductGrid(this.showfev);

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
    final products = showfev ? productsData.fevItems : productsData.items;

    return GridView.builder(
      padding: const EdgeInsets.all(10.0),
      itemCount: products.length,
      itemBuilder: (ctx, index) => ChangeNotifierProvider.value(
       // create: (c) => products[index],
        value: products[index],
        child: ProductItem(),
      ),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10),
    );
  }
}
