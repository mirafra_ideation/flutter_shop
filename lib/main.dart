import 'package:flutter/material.dart';
import 'package:flutter_shops/helpers/CustomRoute.dart';
import 'package:flutter_shops/provider/Auth.dart';
import 'package:flutter_shops/provider/Cart.dart';
import 'package:flutter_shops/provider/Oders.dart';
import 'package:flutter_shops/screens/AuthScreen.dart';
import 'package:flutter_shops/screens/CartScreen.dart';
import 'package:flutter_shops/screens/EditProductScreen.dart';
import 'package:flutter_shops/screens/OdersScreen.dart';
import 'package:flutter_shops/screens/ProductDetailScreen.dart';
import 'package:flutter_shops/screens/ProductOverviewScreen.dart';
import 'package:flutter_shops/screens/SplashScreen.dart';
import 'package:flutter_shops/screens/UserProductsScreen.dart';
import 'provider/Products.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => Auth(),
        ),
        ChangeNotifierProxyProvider<Auth, Products>(
          update: (ctx, auth, previousProducts) => Products(
              auth.userId,
              auth.token,
              previousProducts == null ? [] : previousProducts.items),
        ),
        ChangeNotifierProvider(
          create: (ctx) => Cart(),
        ),
        ChangeNotifierProxyProvider<Auth, Orders>(
          update: (ctx, auth, previousOrder) => Orders(auth.token, auth.userId,
              previousOrder == null ? [] : previousOrder.orders),
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, childchangewidget) => MaterialApp(
          title: 'KHM Homeopathy',
          theme: ThemeData(
              primarySwatch: Colors.purple,
              accentColor: Colors.deepOrange,
              fontFamily: 'Lato',
            pageTransitionsTheme: PageTransitionsTheme(builders: {
              TargetPlatform.android : CustomPageTransitionBuilder(),
              TargetPlatform.iOS : CustomPageTransitionBuilder()
            })
          ),
          home: auth.isAuth
              ? ProductOverviewScreen()
              : FutureBuilder(
                  future: auth.tryAutoLogin(),
                  builder: (ctx, authResultSanpShot) =>
                      authResultSanpShot.connectionState ==
                              ConnectionState.waiting
                          ? SplashScreen()
                          : AuthScreen(),
                ),
          routes: {
            ProductDetailScreen.routeName: (context) => ProductDetailScreen(),
            CartScreen.routeName: (context) => CartScreen(),
            OdersScreen.routeName: (context) => OdersScreen(),
            UserProductsScreen.routeName: (context) => UserProductsScreen(),
            EditProductScreen.routeName: (context) => EditProductScreen()
          },
        ),
      ),
    );
  }
}
