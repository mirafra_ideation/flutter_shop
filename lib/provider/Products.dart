import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shops/models/HttpException.dart';
import 'package:flutter_shops/provider/Product.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Products with ChangeNotifier {
  final String authToken;

  final String userId;

  List<Product> _items = [];

  List<Product> get items {
    return [..._items];
  }
  Products(this.userId,this.authToken, this._items);

  List<Product> get fevItems {
    return _items.where((element) => element.isFavorite).toList();
  }

  Product findById(String id) {
    return _items.firstWhere((element) => element.id == id);
  }

  // [] in method parm is used as optional argument
  Future<void> fetchAndSetProducts([bool filterByUser = false]) async {
    final filterString = filterByUser ? 'orderBy="creatorId"&equalTo="$userId"' : '';
    var url =
        'https://flutter-shop-48acc-default-rtdb.firebaseio.com/products.json?auth=$authToken&$filterString';
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      final List<Product> loadedProduct = [];
      if (extractedData == null) {
        return;
      }

      url = 'https://flutter-shop-48acc-default-rtdb.firebaseio.com/userFavorites/$userId.json?auth=$authToken';
      final fevResponse  = await http.get(url);
      final fevData  = json.decode(fevResponse.body);


      extractedData.forEach((key, product) {
        loadedProduct.add(Product(
            id: key,
            title: product['title'],
            description: product['description'],
            price: product['price'],
            imageUrl: product['imageUrl'],
            isFavorite: fevData == null ? false: fevData[key] ?? false));
      });

      _items = loadedProduct;
      print(json.decode(response.body));
      notifyListeners();
    } catch (error) {
      throw (error);
    }
  }

  Future<void> addProduct(Product product) async {
    final url =
        'https://flutter-shop-48acc-default-rtdb.firebaseio.com/products.json?auth=$authToken';
    try {
      final response = await http.post(url,
          body: json.encode({
            'title': product.title,
            'description': product.description,
            'price': product.price,
            'imageUrl': product.imageUrl,
            'creatorId': userId,
          }));
      print(json.decode(response.body));
      final newProduct = Product(
        title: product.title,
        description: product.description,
        price: product.price,
        imageUrl: product.imageUrl,
        id: json.decode(response.body)['name'],
      );
      _items.add(newProduct);

      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> updateProduct(String id, Product newProduct) async {
    final url =
        'https://flutter-shop-48acc-default-rtdb.firebaseio.com/products/$id.json?auth=$authToken';
    await http.patch(url,
        body: json.encode({
          'title': newProduct.title,
          'description': newProduct.description,
          'price': newProduct.price,
          'imageUrl': newProduct.imageUrl,
        }));

    final prductIndex = _items.indexWhere((element) => element.id == id);
    if (prductIndex >= 0) {
      _items[prductIndex] = newProduct;
      notifyListeners();
    } else {
      print('..no prodcu');
    }
  }

  Future<void> deleteProduct(String id) async {
    final url =
        'https://flutter-shop-48acc-default-rtdb.firebaseio.com/products/$id.json?auth=$authToken';
    final existingproductIndex =
        _items.indexWhere((element) => element.id == id);
    var existingProduct = _items[existingproductIndex];

    _items.removeAt(existingproductIndex);
    notifyListeners();
    final response = await http.delete(url);
    if (response.statusCode >= 400) {
      _items.insert(existingproductIndex, existingProduct);
      notifyListeners();
      throw HttpException('Could not delete product');
    }
    existingProduct = null;
  }
}
