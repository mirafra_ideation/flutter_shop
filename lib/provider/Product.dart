import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class Product with ChangeNotifier{
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({@required this.id, this.title, this.description, this.price, this.imageUrl,
      this.isFavorite = false});

  Future<void> toggleFavoriteStatus(String authToken, String userId) async{
    final url = 'https://flutter-shop-48acc-default-rtdb.firebaseio.com/userFavorites/$userId/$id.json?auth=$authToken';
    final oldStatus = isFavorite;
    isFavorite =!isFavorite;
    notifyListeners();
    try {
      final response = await http.put(url, body: json.encode(isFavorite));

      if(response.statusCode >=400){
        setFevValue(oldStatus);
      }
    }catch(error){
      setFevValue(oldStatus);



    }
  }

  void setFevValue(bool oldStatus) {
     isFavorite = oldStatus;
    notifyListeners();
  }
}