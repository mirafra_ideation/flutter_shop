import 'package:flutter/material.dart';
import 'package:flutter_shops/provider/Cart.dart';
import 'package:flutter_shops/provider/Products.dart';
import 'package:flutter_shops/screens/CartScreen.dart';
import 'package:flutter_shops/widgets/AppDrawer.dart';
import 'package:flutter_shops/widgets/ProductGrid.dart';
import 'package:flutter_shops/widgets/badge.dart';
import 'package:provider/provider.dart';

enum FilterOption { Favorite, All }

class ProductOverviewScreen extends StatefulWidget {
  @override
  _ProductOverviewScreenState createState() => _ProductOverviewScreenState();
}

class _ProductOverviewScreenState extends State<ProductOverviewScreen> {
  var _showFavData = false;
  var _isInit = true;
  var _isLoading = false;
  @override
  void initState() {

    super.initState();
  }
  @override
  void didChangeDependencies() {

    if(_isInit){
      setState(() {
        _isLoading = true;
      });

      Provider.of<Products>(context,listen: false).fetchAndSetProducts().then((value) {
        setState(() {
          _isLoading = false;
        });

      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('KHM Homeopathy'),
        actions: [
          PopupMenuButton(
              onSelected: (FilterOption item) {
                print("item selcted for " + item.toString());
                setState(() {
                  if (FilterOption.Favorite == item) {
                    _showFavData = true;
                  } else {
                    _showFavData = false;
                  }
                });
              },
              itemBuilder: (conntext) => [
                    PopupMenuItem(
                      child: Text("Fav Item"),
                      value: FilterOption.Favorite,
                    ),
                    PopupMenuItem(
                      child: Text("All Item"),
                      value: FilterOption.All,
                    ),
                  ]),
          Consumer<Cart>(
              builder: (context, cartData, ch) =>
                  Badge(child: ch, value: cartData.itemCount.toString()),
              child: IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () {
                  Navigator.of(context).pushNamed(CartScreen.routeName);
                },
              ))
        ],
      ),
      drawer: AppDrawer(),
      body: _isLoading ? Center(child: CircularProgressIndicator(),):ProductGrid(_showFavData),
    );
  }
}
