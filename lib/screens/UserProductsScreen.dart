import 'package:flutter/material.dart';
import 'package:flutter_shops/provider/Products.dart';
import 'package:flutter_shops/screens/EditProductScreen.dart';
import 'package:flutter_shops/widgets/AppDrawer.dart';
import 'package:flutter_shops/widgets/UserProductItem.dart';
import 'package:provider/provider.dart';

class UserProductsScreen extends StatelessWidget {
  static const routeName = '/user-products';

  Future<void> _refreshProduct(BuildContext context) async {
    await Provider.of<Products>(context, listen: false)
        .fetchAndSetProducts(true);
  }

  @override
  Widget build(BuildContext context) {
    // final productsData = Provider.of<Products>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Products'),
        actions: [
          IconButton(
              icon: const Icon(Icons.add),
              onPressed: () {
                Navigator.of(context).pushNamed(EditProductScreen.routeName);
              })
        ],
      ),
      drawer: AppDrawer(),
      body: FutureBuilder(
        future: _refreshProduct(context),
        builder: (ctx, data) => data.connectionState == ConnectionState.waiting
            ? Center(
                child: CircularProgressIndicator(),
              )
            : RefreshIndicator(
                onRefresh: () => _refreshProduct(context),
                child: Padding(
                  padding: EdgeInsets.all(8),
                  child: Consumer<Products>(
                    builder: (ctx, productsData, olddata) => ListView.builder(
                      itemCount: productsData.items.length,
                      itemBuilder: (ctx, index) => Column(
                        children: [
                          UserProductItem(
                              productsData.items[index].id,
                              productsData.items[index].title,
                              productsData.items[index].imageUrl),
                          Divider()
                        ],
                      ),
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
