import 'package:flutter/material.dart';
import 'package:flutter_shops/provider/Oders.dart' show Orders;
import 'package:flutter_shops/widgets/AppDrawer.dart';
import 'package:flutter_shops/widgets/OrderItem.dart';
import 'package:provider/provider.dart';

class OdersScreen extends StatelessWidget {
  static const routeName = '/orders';

  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    // Future.delayed(Duration.zero).then((value) async {
    //   setState(() {
    //     _isLoading = true;
    //   });
    //   print('async call');
    //   await Provider.of<Orders>(context,listen: false).fetchAndSetOrders();
    //   print('await call');
    //   setState(() {
    //     _isLoading = false;
    //   });
    // });
   // final ordersData = Provider.of<Orders>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Orders'),
      ),
      drawer: AppDrawer(),
      body: FutureBuilder(future: Provider.of<Orders>(context,listen: false).fetchAndSetOrders(),
        builder: (ctx, dataSnapShot) {

        if(dataSnapShot.connectionState == ConnectionState.waiting){

        return Center(child: CircularProgressIndicator());
        } else {

          if(dataSnapShot.error != null){
            // do error handling
            return Center(child: Text('An error occur'),);
          }else{
            return Consumer<Orders>(builder: (ctx,ordersData, child) => ListView.builder(
              itemCount: ordersData.orders.length,
              itemBuilder: (ctx, i) => OrderItem(ordersData.orders[i]),
            ),
            ) ;
          }
        }
      },)
    );
  }
}
